//
//  main.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Use Objective C to implement and test the example of Java
//  class RationalNumber available in chapter 7 of your Java
//  textbook. Be sure to include the driver containing the same
//  tests as in your Java textbook.
//
//  INPUTS:    none
//  OUTPUTS:   results of tests that use all created methods
//
// ***********************************************************************
//
//  Problem Statement 2:
//  Create modified versions of the Stars program to print the following
//  patterns. Create a separate program to produce each pattern.
//  Hint: Parts b, c, and d require several loops, some of which print a
//  specific number of spaces.
//
//  Inputs:  a user enters a number
//  Outputs: shapes parameterized by the user input
//
// ***********************************************************************


#import <Foundation/Foundation.h>
#import "Driver1.h"
#import "Driver2.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool {
    // Drawing the shapes
    Driver1 * d1 = [[Driver1 alloc] init];
    [d1 run];

    // Convert Java to Objective-c
    Driver2 * d2 = [[Driver2 alloc] init];
    [d2 run];
  }
  return 0;
}
