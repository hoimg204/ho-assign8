//
//  ConsoleLine.m
//  assign-5-2-2013
//
//  Created by Frank  Niscak on 2014-06-19.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsoleLine : NSObject

- (NSString *) getConsoleLine;
- (NSArray *) getNSArrayOfLinesFromFolder:(NSString *)folderName andFilename:(NSString *)fileName;
- (NSString *) getCompleteFileNameFromFolder:(NSString *)folderName andFileName:(NSString *)fileName;
- (BOOL) writeArrayOfStringsToXMLFile:(NSArray *)data toFolder:(NSString *)folderName toFile:(NSString *)fileName;
- (NSArray *) readArrayOfStringsFromXMLFileFromFolder:(NSString *)folderName fromFile:(NSString *)fileName;

@end