//
//  RationalNumber.h
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Use Objective C to implement and test the example of Java
//  class RationalNumber available in chapter 7 of your Java
//  textbook. Be sure to include the driver containing the same
//  tests as in your Java textbook.
//
//  INPUTS:    none
//  OUTPUTS:   results of tests that use all created methods
//
// ***********************************************************************

#import <Foundation/Foundation.h>

@interface RationalNumber : NSObject

@property int numerator, denominator;

- (id) initWithNumerator:(int)iNum andDenominator:(int)iDenom;
- (RationalNumber *) reciprocal;
- (RationalNumber *) add:(RationalNumber *)op2;
- (RationalNumber *) subtract:(RationalNumber *)op2;
- (RationalNumber *) multiply:(RationalNumber *)op2;
- (RationalNumber *) divide:(RationalNumber *)op2;
- (bool) isLike:(RationalNumber *)op2;
- (int) gcd:(int)num1 :(int)num2;
- (void) reduce;
- (NSString *) description;

@end
