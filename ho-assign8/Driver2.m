//
//  Driver2.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Use Objective C to implement and test the example of Java
//  class RationalNumber available in chapter 7 of your Java
//  textbook. Be sure to include the driver containing the same
//  tests as in your Java textbook.
//
//  INPUTS:    none
//  OUTPUTS:   results of tests that use all created methods
//
// ***********************************************************************

#import "Driver2.h"
#import "RationalNumber.h"

@implementation Driver2

- (void) run
{
  RationalNumber * r1 = [[RationalNumber alloc] initWithNumerator:6 andDenominator:8];
  RationalNumber * r2 = [[RationalNumber alloc] initWithNumerator:1 andDenominator:3];
  RationalNumber * r3, * r4, * r5, * r6, * r7;

  NSLog(@"First rational number: %@", r1);
  NSLog(@"Second rational number: %@", r2);

  if ([r1 isLike:r2])
  {
    NSLog(@"r1 and r2 are equal.");
  }
  else
  {
    r3 = [r1 reciprocal];
  }

  NSLog(@"The reciprocal of r1 is: %@", r3);

  r4 = [r1 add:r2];
  r5 = [r1 subtract:r2];
  r6 = [r1 multiply:r2];
  r7 = [r1 divide:r2];

  NSLog(@"r1 , r2: %@", r4);
  NSLog(@"r1 - r2: %@", r5);
  NSLog(@"r1 * r2: %@", r6);
  NSLog(@"r1 / r2: %@", r7);

}

@end


