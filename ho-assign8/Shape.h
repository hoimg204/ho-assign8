//
//  Shape.h
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Create modified versions of the Stars program to print the following
//  patterns. Create a separate program to produce each pattern.
//  Hint: Parts b, c, and d require several loops, some of which print a
//  specific number of spaces.
//
//  Inputs:  a user enters a number
//  Outputs: shapes parameterized by the user input
//
// ***********************************************************************

#import <Foundation/Foundation.h>

@interface Shape : NSObject

- (void) drawShapeLetterA:(int)size;
- (void) drawShapeLetterB:(int)size;
- (void) drawShapeLetterC:(int)size;
- (void) drawShapeLetterD:(int)size;

@end
