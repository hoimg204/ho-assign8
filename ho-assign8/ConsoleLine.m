
//
//  ConsoleLine.m
//  assign-8-3-2013
//
//  Created by Frank  Niscak on 2014-07-22.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import "ConsoleLine.h"

@implementation ConsoleLine

- (NSString *) getConsoleLine
{
  NSFileHandle * input = [NSFileHandle fileHandleWithStandardInput];
  NSData * inputData = [input availableData];
  NSString * str = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

  str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

  return str;
}

- (NSArray *) getNSArrayOfLinesFromFolder:(NSString *)folderName andFilename:(NSString *)fileName
{
  NSError * error;

  NSString * filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"workspace-ios/datafiles/"];

  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:folderName];
  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:fileName];
  NSLog(@"The file path is : %@", filePath);

  NSString * fileContents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];

  if (error)
  {
    NSLog(@"Error reading file: %@", error.localizedDescription);
  }

  NSArray * listArray = [fileContents componentsSeparatedByString:@"\n"];

  // maybe for debugging...
  // NSLog(@"contents: %@", fileContents);
  // NSLog(@"items = %lu", (unsigned long) [listArray count]);

  return listArray;
}

- (NSString *) getCompleteFileNameFromFolder:(NSString *)folderName andFileName:(NSString *)fileName
{
  NSString * filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"workspace-ios/datafiles/"];

  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:folderName];
  filePath = [filePath stringByAppendingString:@"/"];
  filePath = [filePath stringByAppendingString:fileName];
  return filePath;
}

- (BOOL) writeArrayOfStringsToXMLFile:(NSArray *)data toFolder:(NSString *)folderName toFile:(NSString *)fileName
{
  NSString * filePath = [self getCompleteFileNameFromFolder:folderName andFileName:fileName];
  BOOL success = [data writeToFile:filePath atomically:YES];

  return success;;
}

- (NSArray *) readArrayOfStringsFromXMLFileFromFolder:(NSString *)folderName fromFile:(NSString *)fileName
{
  NSString * filePath = [self getCompleteFileNameFromFolder:folderName andFileName:fileName];
  NSArray * array = [NSArray arrayWithContentsOfFile:filePath];

  return array;
}

@end
