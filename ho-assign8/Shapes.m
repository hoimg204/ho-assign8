//
//  Shape.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Create modified versions of the Stars program to print the following
//  patterns. Create a separate program to produce each pattern.
//  Hint: Parts b, c, and d require several loops, some of which print a
//  specific number of spaces.
//
//  Inputs:  a user enters a number
//  Outputs: shapes parameterized by the user input
//
// ***********************************************************************

#import "Shape.h"

@implementation Shape

-(id) drawShapeLetterA:(int) size
{
    for (int row = size; row >= 0; row--)
    {
        
        for (int star = 1; star <= row; star++)
            NSLog(@"*");
        
        NSLog(@"\n");
        
    }
}

-(id) drawShapeLetterB:(int) size
{
    for (int row = 0; row <= size; row++)
    {
        
        for (int space = size; space >= 0; space--)
        {
            if (row >= space)
                NSLog(@"*");
            else
                NSLog(@" ");
        }
        
        NSLog(@"\n");
        
    }
}

-(id) drawShapeLetterC:(int) size
{
    for (int row = 0; row <= size; row++)
    {
        
        for (int space = size; space >= 0; space--)
        {
            if (row >= space)
                NSLog(@" ");
            else
                NSLog(@"*");
        }
        
        NSLog(@"\n");
        
    }
    
}

-(id) drawShapeLetterD:(int) size
{
    //auxiliary variables
    NSString *star;
    size /= 2;
    
    //Top of pyramid
    for (int i = 0; i < size; i++)
    {
        star = @"*";
        
        for (int j = 0; j < i; j++)
            //star = "*" + star + "*";
            star = [star stringByAppendingString:@"*"];
        star = [star stringByAppendingFormat:@"%@", star];
        star = [star stringByAppendingString:@"*"];
        
        for (int j = 0; j < size - i; j++)
            //star = " " + star + " ";
            star = [star stringByAppendingString:@" "];
        star = [star stringByAppendingFormat:@"%@", star];
        star = [star stringByAppendingString:@" "];
        
        NSLog(@"%@",star);
    }
    
    //Bottom of pyramid
    for (int i = size - 1; i >= 0; i--)
    {
        star = @"*";
        
        for (int j = 0; j < i; j++)
            //star = "*" + star + "*";
            star = [star stringByAppendingString:@"*"];
        star = [star stringByAppendingFormat:@"%@", star];
        star = [star stringByAppendingString:@"*"];
        
        for (int j = 0; j < size - i; j++)
            //star = " " + star + " ";
            star = [star stringByAppendingString:@" "];
        star = [star stringByAppendingFormat:@"%@", star];
        star = [star stringByAppendingString:@" "];
        
        NSLog(@"%@",star);
    }
}

@end
