//
//  Driver1.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Create modified versions of the Stars program to print the following
//  patterns. Create a separate program to produce each pattern.
//  Hint: Parts b, c, and d require several loops, some of which print a
//  specific number of spaces.
//
//  Inputs:  a user enters a number
//  Outputs: shapes parameterized by the user input
//
// ***********************************************************************

#import "Driver1.h"
#import "Shape.h"
#import "ConsoleLine.h"

@implementation Driver1 : NSObject

- (void) run
{
  Shape * shape = [[Shape alloc] init];

  ConsoleLine * console = [[ConsoleLine alloc] init];

  NSLog(@"ENTER SIZE:");
  NSString * strValue = [console getConsoleLine];

  int iValue = [strValue intValue];

  NSLog(@"Letter A:");
  [shape drawShapeLetterA:iValue];
  NSLog(@"Letter B:");
  [shape drawShapeLetterB:iValue];
  NSLog(@"Letter C:");
  [shape drawShapeLetterC:iValue];
  NSLog(@"Letter D:");
  [shape drawShapeLetterD:iValue];

}

@end
