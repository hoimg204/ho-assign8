//
//  RationalNumber.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 1:
//  Use Objective C to implement and test the example of Java
//  class RationalNumber available in chapter 7 of your Java
//  textbook. Be sure to include the driver containing the same
//  tests as in your Java textbook.
//
//  INPUTS:    none
//  OUTPUTS:   results of tests that use all created methods
//
// ***********************************************************************


#import "RationalNumber.h"

@implementation RationalNumber

@synthesize numerator, denominator;

- (id) initWithNumerator:(int)iNum andDenominator:(int)iDenom
{
  self = [super init];
  if (self)
  {
    if (iDenom == 0)
    {
      iDenom = 1;
    }

    //  Make the numerator "store" the sign
    if (iDenom < 0)
    {
      iNum = iNum * -1;
      iDenom = iDenom * -1;
    }

    numerator = iNum;
    denominator = iDenom;

    [self reduce];
  }
  return self;
}

- (RationalNumber *) reciprocal
{
  return [[RationalNumber alloc] initWithNumerator:numerator andDenominator:denominator];
}

- (RationalNumber *) add:(RationalNumber *)op2
{
  int commonDenominator = denominator * op2.denominator;
  int numerator1 = numerator * op2.denominator;
  int numerator2 = op2.numerator * denominator;
  int sum = numerator1 + numerator2;

  return [[RationalNumber alloc] initWithNumerator:sum andDenominator:commonDenominator];
}

- (RationalNumber *) subtract:(RationalNumber *)op2
{
  int commonDenominator = denominator * op2.denominator;
  int numerator1 = numerator * op2.denominator;
  int numerator2 = op2.numerator * denominator;
  int difference = numerator1 - numerator2;

  return [[RationalNumber alloc] initWithNumerator:difference andDenominator:commonDenominator];
}

- (RationalNumber *) multiply:(RationalNumber *)op2
{
  int numer = numerator * op2.numerator;
  int denom = denominator * op2.denominator;

  return [[RationalNumber alloc] initWithNumerator:numer andDenominator:denom];
}

- (RationalNumber *) divide:(RationalNumber *)op2
{
  return [self multiply:op2];
}

- (bool) isLike:(RationalNumber *)op2
{
  return ( numerator == op2.numerator && denominator == op2.denominator );
}

- (int) gcd:(int)num1 :(int)num2
{
  while (num1 != num2)
  {
    if (num1 > num2)
    {
      num1 = num1 - num2;
    }
    else
    {
      num2 = num2 - num1;
    }
  }
  return num1;
}

- (void) reduce
{
  if (numerator != 0)
  {
    int common = [self gcd:abs(numerator):denominator];

    numerator = numerator / common;
    denominator = denominator / common;
  }
}

- (NSString *) description
{
  NSString * result = @"";

  if (numerator == 0)
  {
    result = @"0";
  }
  else
  if (denominator == 1)
  {
    result = [result stringByAppendingFormat:@"%i ", numerator];
  }
  else
  {
    result = [result stringByAppendingFormat:@"%i ", numerator];
  }
  result = [result stringByAppendingString:@"/"];
  result = [result stringByAppendingFormat:@"%i ", denominator];

  return result;
}

@end
