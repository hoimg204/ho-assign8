//
//  Shape.m
//  ho-assign8
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-29.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement 2:
//  Create modified versions of the Stars program to print the following
//  patterns. Create a separate program to produce each pattern.
//  Hint: Parts b, c, and d require several loops, some of which print a
//  specific number of spaces.
//
//  Inputs:  a user enters a number
//  Outputs: shapes parameterized by the user input
//
// ***********************************************************************

#import "Shape.h"

@implementation Shape

- (void) drawShapeLetterA:(int)size
{
  for (int row = size; row >= 0; row--)
  {

    for (int star = 1; star <= row; star++)
    {
      printf("*");
    }

    printf("\n");

  }
}

- (void) drawShapeLetterB:(int)size
{
  for (int row = 0; row <= size; row++)
  {

    for (int space = size; space >= 0; space--)
    {
      if (row >= space)
      {
        printf("*");
      }
      else
      {
        printf(" ");
      }
    }

    printf("\n");

  }
}

- (void) drawShapeLetterC:(int)size
{
  for (int row = 0; row <= size; row++)
  {

    for (int space = size; space >= 0; space--)
    {
      if (row >= space)
      {
        printf(" ");
      }
      else
      {
        printf("*");
      }
    }

    printf("\n");

  }
}

- (void) drawShapeLetterD:(int)size
{
  // Top of the pyramid
  for (int i = 1; i <= size; i = i + 2)
  {
    for (int k = 1; k <= size - i; k = k + 2)
    {
      printf(" ");
    }
    for (int j = 1; j <= i; j++)
    {
      printf("*");
    }
    printf("\n");
  }
  // Bottom of the pyramid
  for (int i = size - 1; i >= 1; i = i - 2)
  {
    for (int k = size - i; k >= 0; k = k - 2)
    {
      printf(" ");
    }
    for (int j = 1; j <= i; j++)
    {
      printf("*");
    }
    printf("\n");
  }
}

@end
